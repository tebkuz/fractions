import unittest
from fractions import Fraction


class TestFractions(unittest.TestCase):
    def test_proper_fraction_from_string(self):
        f = Fraction.from_string("3/2")
        self.assertEqual(3, f.n)
        self.assertEqual(2, f.d)

    def test_improper_fraction_from_string(self):
        f = Fraction.from_string("2_1/2")
        self.assertEqual(5, f.n)
        self.assertEqual(2, f.d)

    def test_reduce_fraction_to_whole_number(self):
        f = Fraction(10, 2)
        self.assertEqual("5", str(f))

    def test_reduce_fraction_to_fraction(self):
        f = Fraction(14, 18)
        self.assertEqual("7/9", str(f))

    def test_multiply_fractions(self):
        f1 = Fraction.from_string("1/2")
        f2 = Fraction.from_string("3/6")
        f3 = f1 * f2
        self.assertEqual(3, f3.n)
        self.assertEqual(12, f3.d)

    def test_divide_fractions(self):
        f1 = Fraction.from_string("1/2")
        f2 = Fraction.from_string("3/7")
        f3 = f1 / f2
        self.assertEqual(7, f3.n)
        self.assertEqual(6, f3.d)

    def test_add_fractions(self):
        f1 = Fraction.from_string("1/2")
        f2 = Fraction.from_string("3/7")
        f3 = f1 + f2
        self.assertEqual(13, f3.n)
        self.assertEqual(14, f3.d)

    def test_subtract_fractions(self):
        f1 = Fraction.from_string("1/2")
        f2 = Fraction.from_string("3/7")
        f3 = f1 - f2
        self.assertEqual(1, f3.n)
        self.assertEqual(14, f3.d)


if __name__ == "__main__":
    unittest.main()
