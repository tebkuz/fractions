import re


class Fraction:
    FRACTION_REGEX = re.compile(
        "((?P<whole>\d+)_)?((?P<numerator>\d+)\/(?P<denominator>\d+))"
    )

    @classmethod
    def from_string(cls, str):
        """
        Parses input string and creates a Fraction object
        from the parsed values
        """
        result = re.match(cls.FRACTION_REGEX, str)
        if not result:
            return None

        whole = int(result.group("whole") or 0)
        num = int(result.group("numerator") or 0)
        denom = int(result.group("denominator") or 1)

        return cls((whole * denom) + num, denom)

    def __init__(self, numerator, denominator):
        self.n = numerator
        self.d = denominator

    def __add__(self, f2):
        """
        Performs an addition using input fraction
        """
        n1 = self.n
        d1 = self.d
        n2 = f2.n
        d2 = f2.d
        return Fraction(n1 * d2 + n2 * d1, d1 * d2)

    def __sub__(self, f2):
        """
        Performs a subtraction using input fraction
        """
        n1 = self.n
        d1 = self.d
        n2 = f2.n
        d2 = f2.d
        return Fraction(n1 * d2 - n2 * d1, d1 * d2)

    def __mul__(self, f2):
        """
        Performs a multiplication using input fraction
        """
        n1 = self.n
        d1 = self.d
        n2 = f2.n
        d2 = f2.d
        return Fraction(n1 * n2, d1 * d2)

    def __truediv__(self, f2):
        """
        Performs a division using input fraction
        """
        n1 = self.n
        d1 = self.d
        n2 = f2.n
        d2 = f2.d
        return Fraction(n1 * d2, d1 * n2)

    def _find_gcd(self, a, b):
        """
        Finds the greatest common divisor of two numbers
        """
        while b:
            a, b = b, a % b
        return a

    def reduce(self):
        """
        Reduces numerator and denominator to smallest values
        """
        gcd = self._find_gcd(self.n, self.d)
        n_reduced = int(self.n / gcd)
        d_reduced = int(self.d / gcd)
        return Fraction(n_reduced, d_reduced)

    def __str__(self):
        """
        Returns a string representation of the object. Performs
        a reduction of the numerator and denominator before
        contructing string representation
        """
        reduced = self.reduce()
        mod = reduced.n % reduced.d
        whole = int(reduced.n / reduced.d)
        if mod == 0:
            return f"{whole}"
        elif whole == 0:
            return f"{reduced.n}/{reduced.d}"
        return f"{whole}_{mod}/{reduced.d}"


if __name__ == "__main__":
    REGEX = re.compile(
        "(?P<fraction1>[\d_\/]+)\ (?P<operator>[\+\*\/\-])\ (?P<fraction2>[\d_\/]+)"
    )

    txt = input("?: ")
    result = re.match(REGEX, txt)
    f1_str = result.group("fraction1")
    f2_str = result.group("fraction2")
    f1 = Fraction.from_string(f1_str)
    f2 = Fraction.from_string(f2_str)
    operator = result.group("operator")

    if operator == "+":
        print(f"={f1 + f2}", end="\n")
    elif operator == "-":
        print(f"={f1 - f2}", end="\n")
    elif operator == "*":
        print(f"={f1 * f2}", end="\n")
    elif operator == "/":
        print(f"={f1 / f2}", end="\n")
    else:
        print(f"Unsupported operator: {operator}")

